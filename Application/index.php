<?php

require_once 'vendor/autoload.php';

\GamePedia\DB\Eloquent::start('conf/conf.ini');
$slim = new \Slim\Slim();

use \GamePedia\vue\VueComment;
use \GamePedia\models\Game;

/**
 * Acceuil
 */
$slim->get('/', function () {
    $vue = new \GamePedia\vue\VueAcceuil() ;
    $vue->render() ;
})->name('acceuil');


$slim->get('/prep04', function () {
    $vue = new \GamePedia\controller\ControllerPrep4() ;
    $vue->test() ;
})->name('prep04');


$slim->get('/td04', function () {
    $vue = new \GamePedia\controller\createurComm() ;
    $vue->creerUser() ;
})->name('td04');


$slim->get('/api/games/:idJ', function ($idJ) {
    $vue = new \GamePedia\controller\ControllerGame() ;
    echo $vue->afficherJeu($idJ) ;
})->name('affJeu');



$slim->get('/api/games/:idJ/comments', function ($idJ) {
    $vue = new VueComment() ;
    echo $vue->vueComs($idJ);
});


$slim->get('/api/games', function () use($slim) {
    $vue = new \GamePedia\controller\ControllerGame() ;
    echo $vue->afficher200Jeu( ($slim->request->get()["page"]-1)*200+1 ) ;
})->name('pageAffJeux');


$slim->run();