<?php
/**
 * Created by PhpStorm.
 * User: ulyss
 * Date: 13/03/2019
 * Time: 16:31
 */

namespace GamePedia\models;
use \Illuminate\Database\Eloquent\Model;

class Commentaire extends Model
{
    protected $table='commentaire';
    protected $primaryKey='idCom';
    public $timestamps=false;
	
	public function game() : BelongsTo {
		return $this->BelongsTo('\models\Game','game_id');
	}
	
	public function utilisateur() : BelongsTo {
		return $this->BelongsTo('\models\Utilisateur','utilisateur_idU');
	}
}