<?php

namespace GamePedia\models;
use \Illuminate\Database\Eloquent\Model;

class Game_rating extends Model
{
    protected $table='gameRating';
    protected $primaryKey='id';
    public $timestamps=false;

	public function game() : HasMany {
        return $this->HasMany('mywishlist\models\Game', 'gameRating_id') ;
    }

	public function game() : BelongsTo {
        return $this->BelongsTo('mywishlist\models\Game','game_id') ;
    }
}
