<?php

namespace GamePedia\models;
use \Illuminate\Database\Eloquent\Model;

class Enemies extends Model
{
  protected $table='enemies';
  protected $primaryKey= ['char1_id','char2_id'];
  public $timestamps=false;
  public $incrementing = false;

  protected function getKeyForSaveQuery()
  {

    $primaryKeyForSaveQuery = array(count($this->primaryKey));

    foreach ($this->primaryKey as $i => $pKey) {
      $primaryKeyForSaveQuery[$i] = isset($this->original[$this->getKeyName()[$i]])
      ? $this->original[$this->getKeyName()[$i]]
      : $this->getAttribute($this->getKeyName()[$i]);
    }

    return $primaryKeyForSaveQuery;

  }

  /**
  * Set the keys for a save update query.
  * @param  \Illuminate\Database\Eloquent\Builder  $query
  * @return \Illuminate\Database\Eloquent\Builder
  */
  protected function setKeysForSaveQuery(Builder $query)
  {

    foreach ($this->primaryKey as $i => $pKey) {
      $query->where($this->getKeyName()[$i], '=', $this->getKeyForSaveQuery()[$i]);
    }

    return $query;
  }
}
