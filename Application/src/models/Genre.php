<?php
/**
 * Created by PhpStorm.
 * User: ulyss
 * Date: 13/03/2019
 * Time: 16:31
 */

namespace GamePedia\models;
use \Illuminate\Database\Eloquent\Model;

class Genre extends Model
{
    protected $table='genre';
    protected $primaryKey='id';
    public $timestamps=false;
	
	public function game() : HasMany {
        return $this->HasMany('\models\Game','genre_id');
    }
	
	public function game() : BelongsTo {
		return $this->BelongsTo('\models\Game','game_id');
	}
}