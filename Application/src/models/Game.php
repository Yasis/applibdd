<?php
/**
 * Created by PhpStorm.
 * User: ulyss
 * Date: 13/03/2019
 * Time: 16:31
 */

namespace GamePedia\models;
use \Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    protected $table='game';
    protected $primaryKey='id';
    public $timestamps=false;

    public static function maxId(){

        $game= Game::max('id') ;
        return $game;
    }

	public function theme() : HasMany {
        return $this->HasMany('\models\Theme','game_id');
    }

	public function character() : HasMany {
        return $this->HasMany('\models\Character','game_id');
    }

	public function genre() : HasMany {
        return $this->HasMany('\models\Genre','game_id');
    }

	public function commentaire() : HasMany {
		return $this->HasMany('\models\Commentaire','game_id');
	}

	public function company() : BelongsTo {
        return $this->BelongsTo('\models\Company','company_id');
    }

	public function platform() : BelongsTo {
        return $this->BelongsTo('\models\platform','platform_id');
    }

	public function game_rating() : BelongsTo {
        return $this->BelongsTo('\models\Game_rating','gameRating_id');
    }

}
