<?php
/**
 * Created by PhpStorm.
 * User: ulyss
 * Date: 13/03/2019
 * Time: 16:31
 */

namespace GamePedia\models;
use \Illuminate\Database\Eloquent\Model;

class Character extends Model
{
    protected $table='character';
    protected $primaryKey='id';
    public $timestamps=false;
	
	public function game() : HasMany {
        return $this->HasMany('\models\Game','character_id');
    }
	
	public function game() : BelongsTo {
		return $this->BelongsTo('\models\Game','game_id');
	}
}