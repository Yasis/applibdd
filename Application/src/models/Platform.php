<?php
/**
 * Created by PhpStorm.
 * User: ulyss
 * Date: 13/03/2019
 * Time: 16:31
 */

namespace GamePedia\models;
use \Illuminate\Database\Eloquent\Model;

class Platform extends Model
{
    protected $table='platform';
    protected $primaryKey='id';
    public $timestamps=false;

	public function company() : BelongsTo {
        return $this->BelongsTo('mywishlist\models\Company','company_id') ;
    }

	public function game() : BelongsTo {
        return $this->BelongsTo('mywishlist\models\Game','game_id') ;
    }

	public function game() : HasMany {
        return $this->HasMany('mywishlist\models\Game', 'platform_id') ;
    }
}
