<?php
/**
 * Created by PhpStorm.
 * User: ulyss
 * Date: 13/03/2019
 * Time: 16:31
 */

namespace GamePedia\models;
use \Illuminate\Database\Eloquent\Model;

class Utilisateur extends Model
{
    protected $table='utilisateur';
    protected $primaryKey='idU';
    public $timestamps=false;

	public function commentaire() : HasMany {
        return $this->HasMany('\models\Commentaire','utilisateur_idU');
    }

}
