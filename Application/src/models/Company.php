<?php
/**
 * Created by PhpStorm.
 * User: ulyss
 * Date: 13/03/2019
 * Time: 16:31
 */

namespace GamePedia\models;
use \Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany as HasMany;

final class Company extends Model
{
    protected $table='company';
    protected $primaryKey='id';
    public $timestamps=false;

    public function game() : HasMany {
        return $this->HasMany('\models\Game','company_id');
    }

	public function platform() : HasMany {
        return $this->HasMany('\models\platform','company_id');
    }

	public function game() : BelongsTo {
        return $this->BelongsTo('\models\Game','game_id');
    }
}
