<?php
/**
* Created by PhpStorm.
* User: vince
* Date: 21/11/2018
* Time: 08:57
*/

namespace GamePedia\DB;
use \Illuminate\Database\Capsule\Manager as DB;

class Eloquent{

    public static function start(String $file){
        $db = new DB();
        $db->addConnection(parse_ini_file($file));
        $db->setAsGlobal();
        $db->bootEloquent();
}
}