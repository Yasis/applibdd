<?php
/**
 * Created by PhpStorm.
 * User: claire
 * Date: 25/03/19
 * Time: 15:08
 */

namespace GamePedia\controller;

use GamePedia\models\Game;
use GamePedia\models\Utilisateur as Utilisateur;
use Carbon\Carbon;

class createurComm
{

    public function creer2Ut() {

    }



    public function creerUser()
    {
        $faker = \Faker\Factory::create() ;

        // Choix du genre : homme ou femme ?
        if ( random_int(1 , 2) === 2 )
            $faker->title('female') ;
        else
            $faker->title('male') ;

        // Choix du nom
        $prenom = $faker->firstName ;
        $nom = $faker->lastName ;

        // Choix du mail en fonction de son nom et prenom
        if ( random_int(1 , 2) === 2 )
            $mail = $prenom . "." . $nom . "@" . $faker->freeEmailDomain ;
        else
            $mail = $nom . "." . $prenom . "@" . $faker->freeEmailDomain ;

        // Choix de l'adresse
        $rue = $faker->streetName ;
        $num = random_int(1 , 168) ;
        $ville = $faker->city ;

        // Choix numéro de tel
        $tel = $faker->phoneNumber ;

        // Choix date de naissance
        $dateNaiss = $faker->date($format = "d-m-Y" , $max = '1999-01-01') ;


        // Creation de l'utilisateur + save dans la BD
        $user = new Utilisateur() ;
        $user->nom = $nom ;
        $user->prenom = $prenom ;
        $user->email = $mail ;
        $user->rueAdr = $rue ;
        $user->numAdr = $num ;
        $user->villeAdr = $ville ;
        $user->numeroTel = $tel ;
        $user->dateNaiss = Carbon::parse($dateNaiss)->format('d-m-Y') ;
        $user->save();


        echo "<p>" . $prenom . " " . $nom . "</p>
                <p> mail : " . $mail . "</p><br>
                <p> Adresse : " . $num . " " . $rue . " - " . $ville . "</p>
                <p> Numéro de téléphone : " . $tel . "</p>
                <p> Date de naissance : " . $dateNaiss . "</p>"  ;




        // Création des commentaires de cet Utilisateur
        echo "<br><br>" ;
        for ($i=0 ; $i<random_int(1 , 4) ; $i++)
        {
            $this->creerCom($user) ;
        }
    }
    
    
    
    public function creerCom($user) {

        $faker = \Faker\Factory::create() ;
        // Choix du jeux qui sera commenté
        $idGame = random_int(0 , Game::maxId()) ;

        //Choix de la date de création du commentaire
        $dateCrea = $faker->dateTimeBetween($min = $user->dateNaiss , $max = 'now')->format("d-m-Y") ;

        // Il y a une chance sur 4 pour que le commentaire ai été modifié
        $dateModif = "" ;
        if ( random_int(1 , 4) === 1) {
            $dateModif = $faker->dateTimeBetween($min = $dateCrea , $max = 'now')->format("d-m-Y") ;
        }

        $contenu = $faker->text(random_int(20 , 200)) ;

        $com = new Commentaire();
        $com->contenu = $contenu ;
        $com->created_at = Carbon::parse($dateCrea)->format('d-m-Y') ;
        $com->updated_at = Carbon::parse($dateModif)->format('d-m-Y') ;
        $com->idJ=$idGame;
        $com->idUt=$user->idU;
        $com->save();

        echo "<p>idG=" . $idGame . " idU=" . $user->idU . "</p>" .
             "<p>date de création : " . $dateCrea . "</p>" .
             "<p>date de mise à jour : " . $dateModif . "</p>" .
             "<p>" .$contenu . "</p>" ;
        echo "<br>" ;
    }


}