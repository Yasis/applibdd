<?php
/**
 * Created by PhpStorm.
 * User: claire
 * Date: 26/03/19
 * Time: 16:33
 */

namespace GamePedia\controller;
use GamePedia\models\Game;
use GamePedia\vue\VueGame;

class ControllerGame
{

        public function afficherJeu($idJ) {
            $game = Game::Select("*")->Where("id" , "=" , $idJ)->first() ;
            return VueGame::vueJeuJSON($game) ;
        }



        public function afficher200Jeu($indexDebut) {
            for ($i = $indexDebut ; $i<$indexDebut+200 ; $i++) {
                $s[] = VueGame::vueSimplJeuJSON( Game::Select("*")->Where("id" , "=" , $i)->first() ) ;
            }

            return VueGame::vue200jeux($s) ;
        }

}