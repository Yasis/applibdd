<?php
/**
 * Created by PhpStorm.
 * User: claire
 * Date: 26/03/19
 * Time: 17:35
 */

namespace GamePedia\vue;
use Slim\Slim ;

class Vue
{
    public static function buildHTML($content) {
        $app = Slim::getInstance();
        $css = $app->request->getRootUri()."/src/css.css";
        return <<<HEAD
<head>
    <title>gamepedia</title>
    <link rel="stylesheet" href='$css'>
</head>
<body>
    $content
</body>
HEAD;

    }
}