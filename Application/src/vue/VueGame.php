<?php
/**
 * Created by PhpStorm.
 * User: claire
 * Date: 26/03/19
 * Time: 16:33
 */

namespace GamePedia\vue;

use Slim\Slim;

class VueGame extends Vue
{

    public static function vueJeuJSON($jeu) {
        return self::buildHTML("<section class='descrGame'>" .
            "<p>" .
            "{ <br> " .
            "&nbsp \"id\": " . $jeu["id"] . "<br>" .
            "&nbsp \"name\": " . $jeu["name"] . "<br>" .
            "&nbsp \"alias\": " . $jeu["alias"] . "<br>" .
            "&nbsp \"deck\": " . $jeu["deck"] . "<br>" .
            "&nbsp \"description\": " . $jeu["description"] . "<br>" .
            "&nbsp \"original_release_date\": " . $jeu["original_release_date"] . "<br>" .
            "}</p>" .
            "</section>") ;
    }



    public static function vueSimplJeuJSON($jeu)
    {
        $app = Slim::getInstance();
        $link=$app->urlFor("affJeu",["idJ"=>$jeu["id"]]);
        return self::buildHTML("<section class='descrGame'>" .
            "<p>" .
            "{ <br> " .
            "&nbsp \"id\": " . $jeu["id"] . "<br>" .
            "&nbsp \"name\": " . $jeu["name"] . "<br>" .
            "&nbsp \"alias\": " . $jeu["alias"] . "<br>" .
            "&nbsp \"deck\": " . $jeu["deck"] . "<br>" .
            "\"links\":{<br>".
            "\"self\" :{ \"href\" : \"".$link."\"}<br>}</p>" .
            "}" ) ;
    }


    public static function vue200jeux($tabS)
    {
        $s = "" ;
        foreach ($tabS as $key => $value) {
            if ($key == 199) {
                $s .= $value ;
            }
            else {
                $s .= $value . ",</p></section><br>" ;
            }
        }

        return self::buildHTML( "{ 
                <section class='descr200Games'> <br>\"Games\" : [ <br>" .
                    $s .
            "} ,<br>".
            "</section>");
    }

}