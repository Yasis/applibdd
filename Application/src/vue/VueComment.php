<?php
/**
 * Created by PhpStorm.
 * User: ulyss
 * Date: 28/03/2019
 * Time: 11:13
 */

namespace GamePedia\vue;

use GamePedia\models\Utilisateur;
use GamePedia\models\Commentaire;
use GamePedia\models\Game;
use Slim\Slim;

class VueComment
{
    public function vueCom($com){
        $user=Utilisateur::where("id","=",$com->idUt)->first();
        return <<<END
        <p>
            <strong>$user->prenom,$user->nom</strong>(le $com->created_at) :<br>
            <p>
                $com->contenu
            </p>
        </p>
        
END;

    }

    public function vueComs($id){
        $jeu=Game::where("id","=",$id)->first();
        $coms=$jeu->commentaire();
        $html = "";
        foreach($coms as $c){
            $html.=$this->vueCom($c)."<br>";
        }
        return $html;

    }
}